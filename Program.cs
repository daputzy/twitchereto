﻿using System;
using Eto.Forms;
using TwitcherEto.Frontend.Windows;

namespace TwitcherEto
{
    internal class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Eto.Platform platform;
            
            switch (OsHelper.DetectOs())
            {
                case OsHelper.Os.Linux:
                    platform = new Eto.GtkSharp.Platform();
                    break;
                case OsHelper.Os.Windows:
                    platform = new Eto.Wpf.Platform();
                    break;
                default:
                    throw new Exception("unknown or unsupported operating system");
            }
            
            var app    = new Application(platform);
            var window = new GamesWindow(app);

            window.Init();
            app.Run(window);
        }
    }
}