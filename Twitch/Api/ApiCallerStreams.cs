﻿using RestSharp;

namespace TwitcherEto.Twitch.Api
{
    public class ApiCallerStreams : ApiCaller<Objects.Streams.RootObject>
    {
        public Objects.Streams.RootObject GetTopStreams(string game)
        {
            return GetTopStreams(game, 10);
        }

        public Objects.Streams.RootObject GetTopStreams(string game, int limit)
        {
            return GetTopStreams(game, limit, 0);
        }
        
        public Objects.Streams.RootObject GetTopStreams(string game, int limit, int offset)
        {
            var request = new RestRequest("/kraken/streams", Method.GET);

            request.AddParameter("game", game);
            request.AddParameter("limit", limit);
            request.AddParameter("offset", offset);

            return Call(request);
        }
    }
}