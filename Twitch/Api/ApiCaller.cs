﻿using System;
using System.Net;
using TwitcherEto.Twitch.Objects;
using RestSharp;
using RestSharp.Deserializers;

namespace TwitcherEto.Twitch.Api
{
    public abstract class ApiCaller<T>
    {
        private readonly IRestClient   _client       = new RestClient("https://api.twitch.tv");
        private readonly IDeserializer _deserializer = new JsonDeserializer();

        protected T Call(IRestRequest request)
        {
            // TODO: auslagern
            request.AddHeader("Accept", "application/vnd.twitchtv.v5+json");
            request.AddHeader("Client-ID", "jj7ildgpuryem1hk1h2vb3ofuti4vp0");

            try
            {
                var response = _client.Execute(request);

                if (response == null)
                {
                    throw new ApiException("response is null");
                }

                if (response.ResponseStatus != ResponseStatus.Completed)
                {
                    throw new ApiException("responseStatus != Completed")
                    {
                        ResponseStatus = response.ResponseStatus,
                        HttpStatusCode = response.StatusCode
                    };
                }

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var error = _deserializer.Deserialize<Error.RootObject>(response);
                
                    throw new ApiException("statusCode != 200 OK")
                    {
                        ResponseStatus = response.ResponseStatus,
                        HttpStatusCode = response.StatusCode,
                        Error          = error
                    };
                }
            
                return _deserializer.Deserialize<T>(response);
            }
            catch (Exception ex)
            {
                throw new ApiException("catched exception", ex);
            }
        }
    }
}