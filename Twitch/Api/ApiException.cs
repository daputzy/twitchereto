﻿using System;
using System.Net;
using System.Runtime.Serialization;
using TwitcherEto.Twitch.Objects;
using RestSharp;

namespace TwitcherEto.Twitch.Api
{
    public class ApiException : Exception
    {
        public ApiException()
        {
        }

        public ApiException(string message) : base(message)
        {
        }

        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ResponseStatus ResponseStatus { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public Error.RootObject Error { get; set; }
    }
}