﻿using RestSharp;

namespace TwitcherEto.Twitch.Api
{
    public class ApiCallerGames : ApiCaller<Objects.Games.RootObject>
    {
        public Objects.Games.RootObject GetTopGames()
        {
            return GetTopGames(10);
        }

        public Objects.Games.RootObject GetTopGames(int limit)
        {
            return GetTopGames(limit, 0);
        }
        
        public Objects.Games.RootObject GetTopGames(int limit, int offset)
        {
            var request = new RestRequest("/kraken/games/top", Method.GET);

            request.AddParameter("limit", limit);
            request.AddParameter("offset", offset);

            return Call(request);
        }
    }
}