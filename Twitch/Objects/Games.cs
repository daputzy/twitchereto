﻿using System.Collections.Generic;

namespace TwitcherEto.Twitch.Objects
{
    public class Games
    {
        public class Box
        {
            public string large { get; set; }
            public string medium { get; set; }
            public string small { get; set; }
            public string template { get; set; }
        }

        public class Logo
        {
            public string large { get; set; }
            public string medium { get; set; }
            public string small { get; set; }
            public string template { get; set; }
        }

        public class Game
        {
            public string name { get; set; }
            public int popularity { get; set; }
            public int _id { get; set; }
            public int giantbomb_id { get; set; }
            public Box box { get; set; }
            public Logo logo { get; set; }
            public string localized_name { get; set; }
            public string locale { get; set; }
        }

        public class Top
        {
            public Game game { get; set; }
            public int viewers { get; set; }
            public int channels { get; set; }
        }

        public class RootObject
        {
            public int _total { get; set; }
            public List<Top> top { get; set; }
        }
    }
}