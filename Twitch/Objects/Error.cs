﻿namespace TwitcherEto.Twitch.Objects
{
    public class Error
    {
        public class RootObject
        {
            public string error { get; set; }
            public int status { get; set; }
            public string message { get; set; }
        }
    }
}