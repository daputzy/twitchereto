﻿using System;
using Eto.Forms;

namespace TwitcherEto.Frontend.Models
{
    public class ViewModel
    {
        public byte[] Img { get; set; }
        public int ImgWidth { get; set; }
        public int ImgHeight { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        
        public EventHandler<MouseEventArgs> ClickEventHandler { get; set; } 
    }
}