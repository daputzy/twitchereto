﻿using System.Collections.Generic;
using Eto.Drawing;
using Eto.Forms;
using TwitcherEto.Frontend.Models;

namespace TwitcherEto.Frontend.Renderers
{
    public class Renderer
    {
        private const int Offset = 14;

        private readonly int _maxItemsPerLine;

        public Renderer(int maxItemsPerLine)
        {
            _maxItemsPerLine = maxItemsPerLine;
        }

        public Control Render(List<ViewModel> models)
        {
            var layout = new DynamicLayout
            {
                Spacing = new Size(Offset, Offset),
                Padding = new Padding(Offset, 0, Offset, Offset)
            };

            layout.BeginHorizontal();

            var i = 0;
            
            foreach (var model in models)
            {
                var remainder = (double) i % _maxItemsPerLine;
                
                if (remainder.Equals(0))
                {
                    layout.EndHorizontal();
                    layout.BeginHorizontal();
                }

                var box = new Box(model).Render();

                box.MouseDown += model.ClickEventHandler;
                
                layout.Add(box);
                
                i++;
            }

            layout.EndHorizontal();
            
            return layout;
        }
        
        public class Box
        {
            private readonly ViewModel _viewModel;

            public Box(ViewModel viewModel)
            {
                _viewModel = viewModel;
            }

            public Control Render()
            {
                var box = new DynamicLayout();
            
                box.BeginVertical();

                box.AddAutoSized(new ImageView
                {
                    Image  = new Bitmap(_viewModel.Img),
                    Width  = _viewModel.ImgWidth,
                    Height = _viewModel.ImgHeight
                });
                box.AddAutoSized(new Label
                {
                    Text  = _viewModel.Text1,
                    Width = _viewModel.ImgWidth,
                    Wrap  = WrapMode.None
                });
                box.AddAutoSized(new Label
                {
                    Text = _viewModel.Text2,
                    Width = _viewModel.ImgWidth,
                    Wrap = WrapMode.None
                });

                box.EndVertical();
            
                box.Cursor = new Cursor(CursorType.Pointer);

                return box;
            }
        }
    }
}