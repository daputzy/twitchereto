﻿using System;
using Eto.Forms;
using TwitcherEto.Frontend.Models;
using TwitcherEto.Frontend.Windows;
using TwitcherEto.Twitch.Objects;

namespace TwitcherEto.Frontend.Mappers
{
    public class GameMapper : Mapper<Games.Top>
    {
        private readonly Application _application;

        public GameMapper(Application application)
        {
            _application = application;
        }

        public override ViewModel Map(Games.Top top)
        {
            return new ViewModel{
                Img = GetBytes(top.game.box.medium),
                ImgWidth = 136,
                ImgHeight = 190,
                Text1 = top.game.name,
                Text2 = top.viewers.ToString(),
                ClickEventHandler = GetClickEventHandler(top)
            };
        }

        private EventHandler<MouseEventArgs> GetClickEventHandler(Games.Top top)
        {
            return (sender, args) =>
            {
                if (args.Modifiers != Keys.None) return;
                if (args.Buttons != MouseButtons.Primary) return;

                var window = new StreamsWindow(_application, top.game.name);
                window.Init();
                window.Show();
            };
        }
    }
}