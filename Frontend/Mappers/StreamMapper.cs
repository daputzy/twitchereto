﻿using System;
using Eto.Forms;
using TwitcherEto.Frontend.Models;
using TwitcherEto.Streamlink;
using TwitcherEto.Twitch.Objects;

namespace TwitcherEto.Frontend.Mappers
{
    public class StreamMapper : Mapper<Streams.Stream>
    {
        public override ViewModel Map(Streams.Stream top)
        {
            return new ViewModel{
                Img = GetBytes(top.preview.medium),
                ImgWidth = 320,
                ImgHeight = 180,
                Text1 = top.channel.name,
                Text2 = top.viewers.ToString(),
                ClickEventHandler = GetClickEventHandler(top)
            };
        }

        private EventHandler<MouseEventArgs> GetClickEventHandler(Streams.Stream stream)
        {
            return (sender, args) =>
            {
                if (args.Modifiers != Keys.None) return;
                if (args.Buttons != MouseButtons.Primary) return;
                
                var clm = new CommandLineManager();
                clm.Exec(stream.channel.url);
            };
        }
    }
}