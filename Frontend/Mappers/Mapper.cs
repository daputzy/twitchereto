﻿using System.Net;
using TwitcherEto.Frontend.Models;

namespace TwitcherEto.Frontend.Mappers
{
    public abstract class Mapper<T>
    {
        private readonly WebClient _webClient = new WebClient();
        
        public abstract ViewModel Map(T top);

        protected byte[] GetBytes(string url)
        {
            return _webClient.DownloadData(url);
        }
    }
}