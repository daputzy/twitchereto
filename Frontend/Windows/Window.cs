﻿using Eto.Drawing;
using Eto.Forms;
using Eto.Forms.ThemedControls;

namespace TwitcherEto.Frontend.Windows
{
    public abstract class Window : Form
    {
        protected readonly Application Application;
        
        protected Window(Application application)
        {
            Application = application;

            Resizable   = false;
            Maximizable = false;
            
            KeyUp += (sender, args) =>
            {
                if (args.Modifiers != Keys.None) return;
                if (args.Key != Keys.F5) return;
                
                Content = GenerateSpinner();
                Init();
            }; 
        }

        public abstract void Init();

        protected Control GenerateSpinner()
        {
            var spinner = new Spinner
            {
                Enabled = true,
                Size = new Size(150, 150)
            };
            
            var handler = (ThemedSpinnerHandler) spinner.Handler;
            
            handler.ElementSize = 0.4f;

            return new TableLayout
            {
                Rows = {
                    new TableRow { ScaleHeight = true },
                    new TableRow(
                        new TableCell { ScaleWidth = true },
                        spinner,
                        new TableCell { ScaleWidth = true }
                    ),
                    new TableRow { ScaleHeight = true }
                }
            };
        }
    }
}