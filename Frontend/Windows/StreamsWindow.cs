﻿using System.Collections.Generic;
using System.Threading;
using Eto.Drawing;
using Eto.Forms;
using TwitcherEto.Frontend.Mappers;
using TwitcherEto.Frontend.Models;
using TwitcherEto.Frontend.Renderers;
using TwitcherEto.Twitch.Api;

namespace TwitcherEto.Frontend.Windows
{
    public class StreamsWindow : Window
    {
        // TODO: konfigurierbar machen
        private const int MaxItems = 20;
        private const int MaxItemsPerLine = 5;

        private readonly string _game;
        
        public StreamsWindow(Application application, string game) : base(application)
        {
            _game = game;
            
            Title = "Twitcher - Top Streams: " + game;
            ClientSize = new Size(1684, 918);

            Content = GenerateSpinner();
        }

        public override void Init()
        {
            var thread = new Thread(() =>
            {
                var api = new ApiCallerStreams();
                var streams = api.GetTopStreams(_game, MaxItems);
                var renderer = new Renderer(MaxItemsPerLine);
                    
                var mapper = new StreamMapper();
                var list   = new List<ViewModel>(); 

                foreach (var stream in streams.streams)
                {
                    list.Add(mapper.Map(stream));
                }
                
                Application.AsyncInvoke(() => { Content = renderer.Render(list); });
            });
            
            thread.Start();
        }
    }
}