﻿using System.Collections.Generic;
using System.Threading;
using Eto.Drawing;
using Eto.Forms;
using TwitcherEto.Frontend.Mappers;
using TwitcherEto.Frontend.Models;
using TwitcherEto.Frontend.Renderers;
using TwitcherEto.Twitch.Api;

namespace TwitcherEto.Frontend.Windows
{
    public class GamesWindow : Window
    {
        // TODO: konfigurierbar machen
        private const int MaxItems = 30;
        private const int MaxItemsPerLine = 10;
        
        public GamesWindow(Application application) : base(application)
        {
            Title = "Twitcher - Top Games";
            ClientSize = new Size(1516, 721);

            Content = GenerateSpinner();
        }

        public override void Init()
        {
            var thread = new Thread(() =>
            {
                var api = new ApiCallerGames();
                var games = api.GetTopGames(MaxItems);
                var renderer = new Renderer(MaxItemsPerLine);
                
                var mapper = new GameMapper(Application);
                var list = new List<ViewModel>(); 

                foreach (var game in games.top)
                {
                    list.Add(mapper.Map(game));
                }
                
                Application.AsyncInvoke(() => { Content = renderer.Render(list); });
            });
            
            thread.Start();
        }
    }
}