﻿using System;

namespace TwitcherEto
{
    public class OsHelper
    {
        public enum Os
        {
            Windows,
            Linux,
            Mac,
            Unknown
        }
        
        public static Os DetectOs()
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.MacOSX:
                    return Os.Mac;

                case PlatformID.Unix:
                    return Os.Linux;

                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                case PlatformID.WinCE:
                    return Os.Windows;

                default:
                    return Os.Unknown;
            }
        }
    }
}