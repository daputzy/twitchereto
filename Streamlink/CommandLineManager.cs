﻿using System;
using System.Diagnostics;

namespace TwitcherEto.Streamlink
{
    public class CommandLineManager
    {
        private ProcessStartInfo GenerateProcessInfoWindows(string command)
        {
            return new ProcessStartInfo
            {
                FileName = "cmd.exe",
                Arguments = "/C " + command,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true
            };
        }
        
        private ProcessStartInfo GenerateProcessInfoLinux(string command)
        {
            return new ProcessStartInfo
            {
                FileName = "/bin/sh",
                Arguments = "-c '" + command + "'",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true
            };
        }

        public void Exec(string stream)
        {
            var command = "streamlink " + stream + " best";
            ProcessStartInfo startInfo;

            switch (OsHelper.DetectOs())
            {
                case OsHelper.Os.Windows:
                    startInfo = GenerateProcessInfoWindows(command);
                    break;
                case OsHelper.Os.Linux:
                    startInfo = GenerateProcessInfoLinux(command);
                    break;
                default:
                    throw new Exception("unknown or unsupported operating system");
            }
            
            var executor = new CommandLineExecutor(startInfo);
            executor.Exec();
        }
    }
}