﻿using System.Diagnostics;
using System.Threading;

namespace TwitcherEto.Streamlink
{
    public class CommandLineExecutor
    {
        private readonly ProcessStartInfo _processStartInfo;

        public CommandLineExecutor(ProcessStartInfo processStartInfo)
        {
            _processStartInfo = processStartInfo;
        }

        public void Exec()
        {
            var thread = new Thread(() =>
            {
                var process = new Process
                {
                    StartInfo = _processStartInfo
                };

                process.Start();

                var stdout = new StreamGobbler(process.StandardOutput, false);
                var stderr = new StreamGobbler(process.StandardError, true);

                stdout.Run();
                stderr.Run();
            });
            
            thread.Start();
        }
    }
}