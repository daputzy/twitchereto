﻿using System;
using System.IO;
using System.Threading;

namespace TwitcherEto.Streamlink
{
    public class StreamGobbler
    {
        public readonly StreamReader Stream;
        public readonly bool IsError;

        public StreamGobbler(StreamReader stream, bool isError)
        {
            Stream  = stream;
            IsError = isError;
        }

        public void Run()
        {
            var thread = new Thread(() =>
            {
                while (!Stream.EndOfStream)
                {
                    var output = Stream.ReadLine();

                    if (IsError)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(output);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.WriteLine(output);
                    }
                }
            });
            
            thread.Start();
        }
    }
}